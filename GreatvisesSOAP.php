<?php

require_once 'libs/vendor/autoload.php';

class GreatvisesSOAP
{
    /**
     * 
     * @return string
     */
    function get_items(){
        require_once 'model/ItemModel.php';
        $model = new ItemModel();
        $items = $model->get_items();
    
        return json_encode($items);
    }
    
    /**
     * 
     * @param string $name
     * @return string
     */
    function search_items($name){
        require_once 'model/ItemModel.php';
        $model = new ItemModel();
        
        $items_arr = $model->search($name);
        
        return json_encode($items_arr);
    }
    
    

}

//$ip = "25.16.169.211";
$ip = $_SERVER['HTTP_HOST'];
$serverUrl = "http://". $ip ."/Greatvises/GreatvisesSOAP.php";

$options = [
    'uri' => $serverUrl
];
$server = new Zend\Soap\Server(null, $options);

if (isset($_GET['wsdl'])) {
    $soapAutoDiscover = new \Zend\Soap\AutoDiscover(new \Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence());
    $soapAutoDiscover->setBindingStyle(array('style' => 'document'));
    $soapAutoDiscover->setOperationBodyStyle(array('use' => 'literal'));
    $soapAutoDiscover->setClass('GreatvisesSOAP');
    $soapAutoDiscover->setUri($serverUrl);
    
    header("Content-Type: text/xml");
    echo $soapAutoDiscover->generate()->toXml();
} else {
    $soap = new \Zend\Soap\Server($serverUrl . '?wsdl');
    $soap->setObject(new \Zend\Soap\Server\DocumentLiteralWrapper(new GreatvisesSOAP()));
    $soap->handle();
}
