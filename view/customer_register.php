<form method="post" action="?action=register_customer" autocomplete="off">
    <div class="col-lg-6 col-lg-offset-3">
        <div>
            <h3 class="text-center" >Datos de la empresa</h3>
        </div>
        <div>
            <label for="name">Nombre de la empresa</label>
            <input class="form-control" id="name" name="name" required/>
        </div>
        <br>
        <div>
            <button class="btn btn-primary" type="submit">Registrar</button>
        </div>
        <br/>
        <div>
            <span id="message">
                <?php
                if(isset($vars['result'])){
                    $result = $vars['result'];
                    $message = $result['response'];

                    $result['success'] ? success_message($message) : error_message($message);
                }
                ?>
            </span>
        </div>
    </div>
    
</form>
