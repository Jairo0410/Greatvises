<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const delimiter = ":";

/**
     * 
     * @param string $name
     * @param string $password
     * @param string $ip
     * @return Response $result
     */
    function ask_password($name, $password, $ip){
        
    }
    
    /**
     * 
     * @param string $name Client's name
     * @param string $passwd Temporary password for single transaction
     * @param string $item_id Identifier of item to be bought
     * @param int $quantity Number of units of the item to be bought
     * @return Response $result
     */
    function purchase_item($name, $passwd, $item_id, $quantity){
        
        
    }
    
    //=========================================================================

use Psr\Http\Message\MessageInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require 'libs/vendor/autoload.php';

$app = new Slim\App();

$app->get("/", function (Request $req, Response $res){
    $res->getBody()->write("Welcome to Greatvises WS!");
    return $res;
});

$app->get("/ask_password/name={name}&password={password}&ip={ip}", function (Request $req, Response $res){
    $name = $req->getAttribute('name');
    $password = $req->getAttribute('password');
    $ip = $req->getAttribute('ip');
    
    // validation
    require_once 'model/CustomerModel.php';

    $model = new CustomerModel();
    $customer = $model->find_customer(trim($name), trim($password));

    if(isset($customer)){
        require_once 'libs/MyMethods.php';

        // password creation
        $ip_str = IP_to_str(trim($ip));
        $temp_pass = $ip_str . get_rand_chars(4, $password);

        $validation = "true";
        $response = $temp_pass;

        // password storage
        try{
            $model->set_temp_pass($name, $temp_pass);
        } catch (Exception $ex) {
            $response = $ex->getMessage();
        }

    }else{
        $validation = "false";
        $response = "wrong credentials";
    }
    
    $res->getBody()->write($validation . delimiter . $response);
    
    return $res;
});

$app->get("/purchase/name={name}&password={password}&item_id={item_id}&quantity={quantity}", function (Request $req, Response $res){
    $name = $req->getAttribute('name');
    $password = $req->getAttribute('password');
    $item_id = $req->getAttribute('item_id');
    $quantity = $req->getAttribute('quantity');
    
    try{
        require_once 'model/CustomerModel.php';
        $customer_model = new CustomerModel();

        $customer_model->check_temp_pass($name, $password);

        $validation = "true";

        require_once 'model/ItemModel.php';
        $items_model = new ItemModel();

        $items_model->buy_item($item_id, $quantity);

        $response = "purchase completed";

        $customer_model->drop_temp_pass($name);
    } catch (Exception $ex) {

        if(!isset($validation)){
           $validation = "false"; 
        }

        if(!isset($response)){
            $response = $ex->getMessage();
        }
    }

    $res->getBody()->write($validation . delimiter . $response);
    
    return $res;
});

$app->run();

?>
