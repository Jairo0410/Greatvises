<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MongoConn
 *
 * @author jairo
 */
require_once 'libs/vendor/autoload.php';

class MongoConn {
    
    private $client = null;
    
    public function __construct() {
        require_once 'libs/Config.php';
        $config = Config::singleton();
        
        // Mongo Atlas (cloud)
        $this->client = new MongoDB\Client($config->get('connString'));
        // Local
        //$this->client = new MongoDB\Client();
    }
    
    public static function get_regex($str){
        return new MongoDB\BSON\Regex($str);
    }
    
    /**
     * 
     * @param string $name
     * @return MongoDB\Collection $collection
     */
    public function get_collection($name){
        $config = Config::singleton();
        $collection = $this->client->selectCollection($config->get('dbname'), $name);
        return $collection;
    }
    
}
