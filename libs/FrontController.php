<?php

/**
 * Description of FrontController
 *
 * @author jairo
 */

    class FrontController{
        static function main(){
            require 'libs/View.php';
            require 'libs/configuration.php';
            
            if(!empty($_GET['controller'])){
                $controllerName = $_GET['controller'] . 'Controller';
            }else{
                $controllerName = 'CustomerController';
            }
            
            if(!empty($_GET['action'])){
                $nombreAccion = $_GET['action'];
            }else{
                $nombreAccion = 'home';
            }
            
            $rutaControlador = $config->get('controllerFolder') . $controllerName . '.php';
            
            if(is_file($rutaControlador)){
                require $rutaControlador;
            }else{
                die('Controlador no encontrado - 404 not found');
            }
            
            if( !is_callable( array($controllerName, $nombreAccion) ) ){
                trigger_error($controllerName . '-> ' . $nombreAccion . ' no existe', E_USER_NOTICE);
                return FALSE;
            }
            
            $controller = new $controllerName(); 
            $controller->$nombreAccion();
        } // main
    } // end class

?>

