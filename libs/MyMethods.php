<?php 
    function message ($type, $message){ ?>
    <div class="alert alert-<?php echo $type ?>" role="alert">
        <?php echo $message ?>
    </div>
        <?php
    }
    
    function centered_message($type, $message){
        ?>
    <div class="text-center alert alert-<?php echo $type ?>" role="alert">
        <?php echo $message ?>
    </div>
<?php
        
    }
    
    function error_message($message){
        message('danger', $message);
    }
    
    function success_message($message){
        message('success', $message);
    }
    
    function warning_message($message){
        message('warning', $message);
    }
    
    function info_message($message){
        message('info', $message);
    }
    
    /**
     * 
     * @param string $ip
     * @return string
     */
    function IP_to_str($ip){
        $firstChar = ord('0');
        $lastChar = ord('z');
        
        $result = "";
        
        foreach (explode('.', $ip) as $chunk){
            $pos = intval($chunk) % ($lastChar - $firstChar) + $firstChar;
            $result = $result . chr($pos);
        }

        return $result;
    }

    /**
     * 
     * @param int $quantity
     * @param string $availableChars
     * @return string
     */
    function get_rand_chars($quantity = 1, $availableChars = NULL){
        
        if($availableChars == NULL){
            $availableChars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        
        $result = "";

        while ($quantity > 0){
            $randNum = rand(0, strlen($availableChars)-1);
            $result = $result . $availableChars[$randNum];

            $quantity = $quantity - 1;
        }

        return $result;
    }
?>