<?php

/**
 * Description of View
 *
 * @author jairo
 */

class View {
    public function __construct(){
        
    }// constructor
    
    // con $vars=array() se obliga a usar un arreglo en ese parametro
    public function show($nombreVista, $vars=array()){
        $config = Config::singleton();
        $path = $config->get('viewFolder') . $nombreVista;
        
        if(!is_file($path)){
            trigger_error('Pagina ' . $path . ' no existe', E_USER_NOTICE);
            return FALSE;
        }// if
        
        if(is_array($vars)){
            foreach ($vars as $key=>$value){
                $key = $value;
            } // foreach
        } // if
        
        include "public/app.php";
    }// show
    
}// end class

?>
