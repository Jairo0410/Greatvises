<?php

/**
 * Description of Config
 *
 * @author jairo
 */

    class Config{
        private $vars;
        private static $instance;
        
        private function __construct(){
            $this->vars = array();
        }// constructor
        
        public function set($nombreAtributo, $valor){
            if(!isset($this->vars[$nombreAtributo])){
                $this->vars[$nombreAtributo] = $valor;
            }// if
        }// set
        
        public function get($nombreAtributo){
            if(isset($this->vars[$nombreAtributo])){
                return $this->vars[$nombreAtributo];
            }// if
        }// get
        
        public static function singleton(){
            if(!isset(self::$instance)){
                $tempClass = __CLASS__;
                self::$instance = new $tempClass;
                
                self::$instance->set('controllerFolder', 'controller/');
                self::$instance->set('modelFolder', 'model/');
                self::$instance->set('viewFolder', 'view/');

                self::$instance->set('dbhost', 'greatvises2018-wuv9f.mongodb.net');
                self::$instance->set('dbname', 'greatvises');
                self::$instance->set('dbuser', 'admin');
                self::$instance->set('dbpass', 'admin');
                self::$instance->set('port', 3306);
                
                $connString = 'mongodb+srv://'
                        . self::$instance->get('dbuser') . ':'
                        . self::$instance->get('dbpass') . '@'
                        . self::$instance->get('dbhost')
                        . '/test?retryWrites=true';
                
                self::$instance->set('connString', $connString);
            }// if
            
            return self::$instance;
        }// singleton
        
    } // end class
?>

