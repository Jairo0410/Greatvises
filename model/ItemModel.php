<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemModel
 *
 * @author jairo
 */

class ItemModel {
    const collection = "item";
    protected $db;
    
    const ROUTE_IMG = "Greatvises/public/img/";
    
    public function __construct() {
        require_once 'libs/MongoConn.php';
        $conn = new MongoConn();
        $this->db = $conn->get_collection(self::collection);
    }
    
    /**
     * 
     * @param array $filter
     * @return array $items
     */
    public function get_items($filter = array()){
        $items = $this->db->find($filter)->toArray();
        
        $server = "http://" . $_SERVER['HTTP_HOST']."/";
        
        foreach($items as $doc){
            $doc['image'] = $server . self::ROUTE_IMG . $doc['image'];
            $doc["_id"] = strval($doc["_id"]);
        }
        
        return $items;
    }
    
    /**
     * 
     * @param array $filter
     */
    public function get_item($filter = array()){
        $item = $this->db->findOne($filter);
        
        $server = "http://" . $_SERVER['HTTP_HOST']."/";
        
        if(isset($item)){
            //$item = $item->toArray();
            $doc['image'] = $server . self::ROUTE_IMG . $doc['image'];
            $item['_id'] = strval($item['_id']);
        }
        
        return $item;
    }
    
    public function search($name){
        $regex = MongoConn::get_regex($name);
        return $this->get_items(['name' => $regex]);
    }
    
    /**
     * 
     * @param string $item_id
     * @param int $quantity
     * @throws Exception
     */
    public function buy_item($item_id, $quantity){
        // as _id is unique, only one item, at most, can be returned
        $bson_id = new \MongoDB\BSON\ObjectId($item_id);
        $product = $this->get_item(['_id' => $bson_id]);
        
        if(!isset($product)){
            throw new Exception("item $item_id does not exist");
        }
        
        $stock = $product['quantity'];
        
        if($stock < $quantity){
            throw new Exception("not enough stock, only $stock left");
        }
        
        $this->db->updateOne(
                ['_id' => $bson_id],
                ['$set' => ['quantity' => $stock - $quantity]]
                );
    }
}
