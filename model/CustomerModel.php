<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomerModel{
    const COLLECTION = "customer";
    const NOPASS = 'nopass';
    
    protected $db;
    
    public function __construct() {
        require_once 'libs/MongoConn.php';
        $conn = new MongoConn();
        $this->db = $conn->get_collection(self::COLLECTION);
    }
    
    /**
     * 
     * @param string $name
     * @throws Exception
     */
    public function register_customer($name){
        $result = $this->find_customer($name);
        
        if(!isset($result)){
            require_once 'libs/MyMethods.php';
            $password = strtoupper(substr($name, 0, 5)) . get_rand_chars(5);

            $customer = ["name" => $name, "password" => $password];
            $this->db->insertOne($customer);
            
            return $password;
        }else{
            throw new Exception("Un cliente con el mismo nombre "
                    . "se encuentra ya registrado");
        }
    }
    
    /**
     * 
     * @param string $name
     * @param string pass
     */
    public function set_temp_pass($name, $pass){
        $this->db->updateOne(['name' => $name], ['$set' => ['temp_pass' => $pass]]);
    }
    
    public function drop_temp_pass($name){
        $this->db->updateOne(['name' => $name], ['$set' => ['temp_pass' => self::NOPASS]]);
    }
    
    public function check_temp_pass($name, $pass){
        $customer = $this->find_customer($name);
        
        if(isset($customer)){
            
            if(!isset($customer['temp_pass']) || $customer['temp_pass'] == self::NOPASS){
                throw new Exception("no temporary password, ask for a new password");
            }
            
            if($customer['temp_pass'] != $pass){
                throw new Exception("wrong temp password");
            }
            
        }else{
            throw new Exception("there's no user with that name");
        }
    }
    
    /**
     * 
     * @param string $name
     * @param string $password
     * @return array $customer
     */
    public function find_customer($name, $password = null){
        if($password != null){
            return $this->db->findOne(['name' => $name, 'password' => $password]);
        }else{
            return $this->db->findOne(['name' => $name]);
        }
    }
}
?>