<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>
            <?php
            if(isset($vars['title'])){
                echo $vars['title'];
            }else{
                echo 'Greatvises';
            }
            ?>
        </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="imaage/x-icon" href="public/img/icono.ico"/>
        <link rel="stylesheet" type="text/css" href="public/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="public/css/estilos.css">
    </head>
    <body>
        <script type="text/javascript" src="public/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="public/js/bootstrap.js"></script>
        <?php
        // some useful alert display methods
            include_once 'libs/MyMethods.php'; 
        ?>
        <div id="main">
            <?php include $path; ?>
        </div>
    </body>
</html>

