<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomerController{
    private $view;
    
    const possibleChars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    public function __construct() {
        $this->view = new View();
    }
    
    public function check(){
        $name = $_GET['name'];
        echo $this->is_valid($name);
    }
    /**
     * 
     * @param string $str
     * returns bool
     */
    public function is_valid($str){
        $pssbl_chrs = str_split(self::possibleChars);
        $str_array = str_split($str);
        
        foreach($str_array as $char){
            $found = FALSE;
            
            foreach ($pssbl_chrs as $valid_char){
                if($char == $valid_char) {
                    $found = TRUE;
                    break;
                }
            }
            
            if(!$found){
                return false;
            }
        }
        
        return true;
    }
    
    public function register_customer(){
        $name = $_POST['name'];
        
        require 'model/CustomerModel.php';
        
        $model = new CustomerModel();
        
        try{
            $password = $model->register_customer($name);
            $result['success'] = true;
            $result['response'] = $password;
        } catch (Exception $ex) {
            $result['success'] = false;
            $result['response'] = $ex->getMessage();
        }
        
        $data['result'] = $result;
        $this->view->show("customer_register.php", $data);
    }

    public function home(){
        $data['title'] = 'Greatvies - Registro';
        $this->view->show("customer_register.php", $data);
    }
    
    public function insert_category(){
        require 'model/ItemModel.php';
        
        $name = $_POST['name'];
        
        $model = new ItemModel();
        $data['message'] = $model->insert_category($name);
        
        session_start();
        
        $data['type'] = $_SESSION['type'];
        
        $data['categories'] = $model->get_categories();
        
        $this->view->show("item_insert.php", $data);
    }
    
    public function insert_item(){
        require_once 'model/ItemModel.php';
        
        $model = new ItemModel();
        
        $name = $_POST['name'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        $category = $_POST['category'];
        
        $base_name = basename($_FILES['image']['name']);
        
        $resulting_route = "img/" . $base_name;
        
        $data['message_insert'] = 'El articulo se guardó de manera exitosa';

        $success = move_uploaded_file($_FILES['image']['tmp_name'], $resulting_route);

        if(!$success){
            $data['message_insert'] = 'Hubo un error subiendo la imagen';
        }else{
            $model->insert_item($name, $price, $description, $resulting_route, $category);
        }
            
        $this->item_insert($data);
        
    }
    
    public function item_insert($data = array()){
        
        require_once 'model/ItemModel.php';
        
        $model = new ItemModel();
        
        session_start();
        
        $data['type'] = $_SESSION['type'];
        
        $data['categories'] = $model->get_categories();
        
        $this->view->show("item_insert.php", $data);
        
    }
    
    public function show_items(){
        
        if(session_status() != PHP_SESSION_ACTIVE){
            session_start();
        }
        
        require 'model/ItemModel.php';
        $model = new ItemModel();

        $username = $_SESSION['username'];

        $data['items'] = $model->get_products($username);
        $this->view->show('items.php', $data);
    }
    
    public function show_history(){
        require 'model/ItemModel.php';
        $model = new ItemModel();
        
        session_start();
        
        $data['type'] = $_SESSION['type'];
        
        $data['history'] = $model->show_history();
        
        $this->view->show("history.php", $data);
    }
}