<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemController
 *
 * @author jairo
 */
class ItemController {
    private $view;
    
    public function __construct() {
        $this->view = new View();
    }
    
    public function get_items(){
        require 'model/ItemModel.php';
        
        $model = new ItemModel();
        
        $items = $model->get_items();
        
        echo json_encode($items);
    }
}
